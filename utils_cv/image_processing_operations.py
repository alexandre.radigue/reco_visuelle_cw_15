import numpy as np
import cv2
import argparse
from unittest import *

# def resize_image(img):
#     res = cv2.resize(img, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)


def resize_image(img, new_dim):
    res = cv2.resize(img, new_dim, interpolation=cv2.INTER_CUBIC)
    return (res)


def rotate_image(img, degree):
    (h, w, d) = img.shape
    center = (h/2, w/2)
    rotate_matrix = cv2.getRotationMatrix2D(center, degree, scale=0.5)
    rotated_image = cv2.warpAffine(src=img, M=rotate_matrix, dsize=(w, h))
    return (rotated_image)


def smoothing_image(img, a, b, sigmaX):
    smooth_image = cv2.GaussianBlur(img, (a, b), sigmaX)
    return (smooth_image)


def draw_rectangle(img, tlcorner, brcorner, color, line_thickness):
    rectangled_image = cv2.rectangle(
        img, tlcorner, brcorner, color, line_thickness)
    return (rectangled_image)


# img = cv2.imread("C:/Users/eugid/Desktop/CoursCS/SIPCS/reco_visuelle_cw_15/Data/tetris_blocks.png")
# (h, w, d) = img.shape
# new_dim = (200, 200)
# # when
# resized_image = resize_image(img, new_dim)

# cv2.imshow('image', resized_image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# img = cv2.imread("C:/Users/eugid/Desktop/CoursCS/SIPCS/reco_visuelle_cw_15/Data/tetris_blocks.png")
# degree = 180
# # when
# rotated_image = rotate_image(img, degree)

# cv2.imshow('image', rotated_image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# img = cv2.imread(
#     "C:/Users/eugid/Desktop/CoursCS/SIPCS/reco_visuelle_cw_15/Data/tetris_blocks.png")
# sigmaX = 0
# smoothed_image = smoothing_image(img, 55, 55, sigmaX)
# cv2.imshow('image', smoothed_image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# img = cv2.imread(
#     "C:/Users/eugid/Desktop/CoursCS/SIPCS/reco_visuelle_cw_15/Data/tetris_blocks.png")
# sigmaX = 0
# rectangled_image = draw_rectangle(img, (384, 100), (400, 350), (100, 255, 100), 3)
# cv2.imshow('image', rectangled_image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
