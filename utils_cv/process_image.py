from image_processing_operations import *  # pour importer les images.
# from loading import *
import argparse
import numpy as np
import cv2
import sys


def apply_processing(image, process):
    imagedest = image
    if process == "resizing":
        new_size = input(
            "What is the new size : (two numbers separated by a space)  ?")
        l = new_size.split(" ")  # séparation aux espaces
        dims = []
        for i in l:
            dims.append(int(i))
        new_dims = tuple(dims)  # transforme en tuple ()
        imagedest = resize_image(image, new_dims)

    if process == "rotating":
        new_rotation = input(
            "What is the new degree ?")
        imagedest = rotate_image(image, int(new_rotation))

    elif process == "smoothing":
        new_smoothing = input(
            "What is the new smooth (two numbers separated by a space)  ?")
        l = new_smoothing.split(" ")
        smooth = []
        for i in l:
            smooth.append(int(i))
        new_smooth = tuple(smooth)
        imagedest = smoothing_image(
            image, new_smooth[0], new_smooth[1], 1)

    elif process == "rectangle":
        tlcorner = input(
            "What is the tlcorner (two numbers separated by a space) ?")
        l = tlcorner.split()
        size = []
        for i in l:
            size.append(int(i))
        new_tlcorner = tuple(size)

        blcorner = input(
            "What is the blcorner (two numbers separated by a space) ?")
        l = blcorner.split()
        size = []
        for i in l:
            size.append(int(i))
        new_blcorner = tuple(size)

        color = input(
            "What is the color (three numbers separated by a space) ?")
        l = color.split()
        size = []
        for i in l:
            size.append(int(i))
        new_color = tuple(size)

        line_thickness = input("What is the line thickness ?")

        imagedest = draw_rectangle(
            image, new_tlcorner, new_blcorner, new_color, int(line_thickness))
    return imagedest


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input", required=True, help="path to input image")
    args = vars(ap.parse_args())
    image_source_filename = args["input"]
    image_source = cv2.imread(image_source_filename)
    # dest = apply_processing(image_source, "rectangle")
    # cv2.imwrite("../Data/result.png", dest)
    # dest = apply_processing(image_source, "rotating")
    # cv2.imwrite("../Data/result.png", dest)
    # dest = apply_processing(image_source, "smoothing")
    # cv2.imwrite("../Data/result.png", dest)
    # dest = apply_processing(image_source, "resizing")
    # cv2.imwrite("../Data/result.png", dest)

    for i in ["resizing", "rotating", "smoothing", "rectangle"]:
        dest = apply_processing(
            image_source, i)
        # saves the image cv2.imwrite (filename, image)
        cv2.imwrite(
            "../Data/result_"+i+".png", dest)


# (384, 100), (400, 350), (100, 255, 100), 3


# python .\process_image.py --input ..\Data\tetris_blocks.png
# écrire ca pour éxécuter le fichier python ...


# What is the new size : (two numbers separated by a space)  ?200 200
# What is the new degree ?180
# What is the new smooth(two numbers separated by a space)  ?55 55 0
# What is the tlcorner(two numbers separated by a space) ?384 100                                                              What is the blcorner(two numbers separated by a space) ?400 350
# What is the color(three numbers separated by a space) ?100 255 100
# What is the line thickness ?3
