import numpy as np
import cv2
import argparse
from unittest import *

# def resize_image(img):
#     res = cv2.resize(img, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)


def resize_image(img, new_dim):
    res = cv2.resize(img, new_dim, interpolation=cv2.INTER_CUBIC)
    return(res)

def rotate_image(img,degree):
    rotate_matrix = cv2.getRotationMatrix2D((320,240), degree, scale = 1)
    return(rotate_matrix)

def smoothing_image(img,a,b,sigmaX):
    smooth_image = cv2.GaussianBlur(img, (a,b), sigmaX)
    return(smooth_image)


def draw_rectangle(img, tlcorner, brcorner, color, line_thickness):
    rectangled_image = cv2.rectangle(img, tlcorner, brcorner, color, line_thickness)
    return (rectangled_image)



def test_resize_image():
    # given
    img = cv2.imread("C:/Users/eugid/Desktop/CoursCS/SIPCS/reco_visuelle_cw_15/Data/tetris_blocks.png")
    (h, w, d) = img.shape
    new_dim = (200, 200)
    # when
    resized_image = resize_image(img, new_dim)
    cv2.imshow('image', resized_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    # then
    assert resized_image.shape == (new_dim[0], new_dim[1], d)


def test_rotate_image():
    # given
    img = cv2.imread("C:/Users/eugid/Desktop/CoursCS/SIPCS/reco_visuelle_cw_15/Data/tetris_blocks.png")
    degree = 360
    # when
    rotated_image = rotate_image(img, degree)
    # then
    assert np.array_equal(img, rotated_image) == True
    # given
    degree = 45
    # when
    rotated_image = rotate_image(img, degree)
    # then
    assert np.array_equal(img, rotated_image) == False


def test_smoothing_image():
    img = cv2.imread("C:/Users/eugid/Desktop/CoursCS/SIPCS/reco_visuelle_cw_15/Data/tetris_blocks.png")
    sigmaX = 0
    a=5
    b=5
    smoothed_image = smoothing_image(img, a, b, sigmaX)
    assert np.array_equal(img, smoothed_image) == False


def test_draw_rectangle():
    # given
    img = cv2.imread("C:/Users/eugid/Desktop/CoursCS/SIPCS/reco_visuelle_cw_15/Data/tetris_blocks.png")
    tlcorner = (50, 50)
    brcorner = (100, 100)
    color = (0, 0, 255)
    line_thickness = 1
    #when
    image_with_rectangle = draw_rectangle(img, tlcorner, brcorner, color, line_thickness)
    #then
    for x in range(50, 100):
        assert image_with_rectangle[x, 50, 2] == 255
        assert image_with_rectangle[x, 100, 2] == 255
        assert image_with_rectangle[50, x, 2] == 255
        assert image_with_rectangle[100, x, 2] == 255
